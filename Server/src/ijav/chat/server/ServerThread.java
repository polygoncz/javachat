package ijav.chat.server;

import ijav.chat.library.Message;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedDeque;

public class ServerThread extends Thread {

    private ObjectOutputStream outObj;
    private ObjectInputStream inObj;
    private Socket socket;
    private ConcurrentLinkedDeque<ClientRecord> allClients;

    public ServerThread(ClientRecord cr, ConcurrentLinkedDeque<ClientRecord> allClients) throws IOException {
        outObj = cr.getOutStream();
        inObj = cr.getInStream();
        this.socket = cr.getSocket();
        this.allClients = allClients;
    }

    @Override
    public void run() {
        Message msg;
        try {
            while (!(msg = (Message) inObj.readObject()).getContent().equalsIgnoreCase("stop")) {
                processMessage(msg);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (EOFException e) {
            try {
                socket.close();
                ClientRecord disconnectedClient = null;

                for (ClientRecord cr : allClients) {
                    if (socket == cr.getSocket()) {
                        allClients.remove(cr);
                        disconnectedClient = cr;
                    }
                }

                if (disconnectedClient != null)
                    System.out.println("Uzivatel " + disconnectedClient.getNick() + " byl odpojen.");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("Problem");
        }
    }

    private void processMessage(Message msg) throws IOException {
        if (msg.getContent().equalsIgnoreCase("listOnline")) {
            sendListOnline();
        } else {
            broadcast(msg);
        }
    }

    private void sendListOnline() throws IOException {
        StringBuilder sb = new StringBuilder();

        for (ClientRecord client : allClients) {
            if (client.getNick() != null) {
                sb.append(client.getNick()).append(";;");
            }
        }

        Message msg = new Message(sb.toString(), "listOnline");
        outObj.writeObject(msg);
    }

    private void broadcast(Message msg) throws IOException {
        for (ClientRecord cr : allClients) {
            if (!cr.getSocket().isClosed()) {
                if (cr.getSocket() != socket) {
                    cr.getOutStream().writeObject(msg);
                    cr.getOutStream().flush();
                } else {
                    if (cr.getNick() == null) {
                        cr.setNick(msg.getNickname());
                    }
                }
            }
        }
    }
}
