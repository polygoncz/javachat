package ijav.chat.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedDeque;

public class StartServer {
    public static void main(String[] args) throws IOException {
        int port = 55555;

        ServerSocket serverSocket = new ServerSocket(port);
        Socket socket;
        ClientRecord cr;
        ConcurrentLinkedDeque<ClientRecord> clients = new ConcurrentLinkedDeque<>();

        System.out.println("Cekani na klienta. " + InetAddress.getLocalHost().getHostAddress() + ":" + serverSocket.getLocalPort());
        while(true) {
            synchronized (serverSocket) {
                socket = serverSocket.accept();
                cr = new ClientRecord(socket);
                clients.add(cr);
            }
            new ServerThread(cr, clients).start();
        }
    }
}
