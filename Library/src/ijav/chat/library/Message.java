package ijav.chat.library;

import java.io.Serializable;

public class Message implements Serializable {
    public static final long serialVersionUID = 42L;

    private String content;
    private String nickname;

    public Message(String content, String nickname) {
        this.content = content;
        this.nickname = nickname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "<" + nickname + ">: " + content;
    }
}
