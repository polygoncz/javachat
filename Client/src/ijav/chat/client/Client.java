package ijav.chat.client;

import ijav.chat.library.Message;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author pavel
 */
public class Client extends JFrame {

    public String nick;

    private PrintStream console;
    private Thread clientListenerThread;
    private Socket socket;

    private ObjectOutputStream outCom;
    private ObjectInputStream inCom;

    public Client() {
        initComponents();

        jBtnSend.setEnabled(false);

        jBtnConnect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connectAction();
            }
        });

        jScrollPane2.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());
            }
        });

        jBtnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jAreaMessage.getText().equals("")) {
                    try {
                        Message msg = new Message(jAreaMessage.getText(), nick);
                        outCom.writeObject(msg);
                        outCom.flush();

                        console.println(msg);
                        jAreaMessage.setText("");
                    } catch (IOException e1) {
                        System.err.println("Chyba odesilani.");
                    }
                }
            }
        });
    }

    public void close() {
        try {
            if (socket != null)
                socket.close();
        } catch (IOException e) {
            System.err.println("Unable to close stream.");
        }
    }

    private void connectAction() {
        nick = jTextNick.getText();
        if (nick.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "Vyplnte nick.");
            return;
        }

        try {
            connectToServer();
            jTextAddress.setEnabled(false);
            jTextNick.setEnabled(false);
            jBtnConnect.setEnabled(false);
            jBtnSend.setEnabled(true);
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(this, "Nepovedlo se pripojit k serveru.");
        }
    }

    private void connectToServer() throws IOException {
        console = new PrintStream(new TextAreaOutputStream(jAreaChat));
        socket = new Socket();
        socket.connect(new InetSocketAddress(this.jTextAddress.getText(), 55555), 3750);
        outCom = new ObjectOutputStream(socket.getOutputStream());
        inCom = new ObjectInputStream(socket.getInputStream());

        clientListenerThread = new Thread(new ClientListener(inCom, console, jListOnlineUsers));
        clientListenerThread.setDaemon(true);
        clientListenerThread.start();

        new OnlineRefreshThread(outCom).start();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        jPanelPripojeni = new javax.swing.JPanel();
        jLblAddress = new javax.swing.JLabel();
        jTextAddress = new javax.swing.JTextField();
        jBtnConnect = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextNick = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListOnlineUsers = new javax.swing.JList();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jAreaChat = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jAreaMessage = new javax.swing.JTextArea();
        jBtnSend = new javax.swing.JButton();

        this.setTitle("Chat client");
        jAreaChat.setEditable(false);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelPripojeni.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        jLblAddress.setText("IP adresa serveru:");

        jBtnConnect.setText("Připojit");

        jLabel1.setText("Nick:");

        javax.swing.GroupLayout jPanelPripojeniLayout = new javax.swing.GroupLayout(jPanelPripojeni);
        jPanelPripojeni.setLayout(jPanelPripojeniLayout);
        jPanelPripojeniLayout.setHorizontalGroup(
                jPanelPripojeniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelPripojeniLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLblAddress)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextNick, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBtnConnect)
                                .addContainerGap())
        );
        jPanelPripojeniLayout.setVerticalGroup(
                jPanelPripojeniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPripojeniLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelPripojeniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLblAddress)
                                        .addComponent(jTextAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jBtnConnect)
                                        .addComponent(jLabel1)
                                        .addComponent(jTextNick, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())
        );

        jListOnlineUsers.setBorder(javax.swing.BorderFactory.createTitledBorder("Online"));
        jScrollPane1.setViewportView(jListOnlineUsers);

        jSplitPane1.setDividerLocation(400);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jAreaChat.setColumns(20);
        jAreaChat.setRows(5);
        jScrollPane2.setViewportView(jAreaChat);

        jSplitPane1.setTopComponent(jScrollPane2);

        jPanel1.setLayout(new java.awt.BorderLayout());

        jAreaMessage.setColumns(20);
        jScrollPane4.setViewportView(jAreaMessage);

        jPanel1.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jBtnSend.setText("Odeslat");

        jPanel1.add(jBtnSend, java.awt.BorderLayout.LINE_END);

        jSplitPane1.setRightComponent(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanelPripojeni, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jSplitPane1)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanelPripojeni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jSplitPane1)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE))
                                .addContainerGap())
        );

        pack();
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                final Client mf = new Client();
                mf.setVisible(true);

                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        mf.close();
                    }
                });
            }
        });
    }

    private javax.swing.JTextArea jAreaChat;
    private javax.swing.JTextArea jAreaMessage;
    private javax.swing.JButton jBtnConnect;
    private javax.swing.JButton jBtnSend;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLblAddress;
    private javax.swing.JList jListOnlineUsers;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelPripojeni;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextField jTextAddress;
    private javax.swing.JTextField jTextNick;
}
