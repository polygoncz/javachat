package ijav.chat.client;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class TextAreaOutputStream extends OutputStream {

    /**
     * Represents the data written to the stream.
     */
    ArrayList <Byte> data = new ArrayList <Byte> ();

    /**
     * Represents the text area that will be showing the written data.
     */
    private JTextArea output;

    /**
     * Creates a console context.
     * @param output
     *      The text area to output the consoles text.
     */
    public TextAreaOutputStream(JTextArea output) {
        this.output = output;
    }

    /**
     * Called when data has been written to the console.
     */
    private void fireDataWritten() {

        // First we loop through our written data counting the lines.
        int lines = 0;
        for (int i = 0; i < data.size(); i++) {
            byte b = data.get(i);

            // Specifically we look for 10 which represents "\n".
            if (b == 10) {
                lines++;
            }
        }

        // We then create a string builder to append our text data.
        StringBuilder sb = new StringBuilder();

        // We loop through the text data appending it to the string builder.
        for (byte b : data) {
            sb.append((char) b);
        }

        // Finally we set the outputs text to our built string.
        output.setText(sb.toString());
    }

    @Override
    public void write(int i) throws IOException {

        // Append the piece of data to our array of data.
        data.add((byte) i);

        // Indicate that data has just been written.
        fireDataWritten();
    }
}
