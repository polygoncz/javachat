package ijav.chat.client;

import ijav.chat.library.Message;

import java.io.IOException;
import java.io.ObjectOutputStream;

public class OnlineRefreshThread extends Thread {

    public static final int PERIOD = 5000;

    private ObjectOutputStream outCom;

    public OnlineRefreshThread(ObjectOutputStream outObj) {
        this.outCom = outObj;
        this.setDaemon(true);
    }

    @Override
    public void run() {
        while(true) {
            Message msg = new Message("listOnline", "client-request");
            try {
                outCom.writeObject(msg);
            } catch (IOException e) {
                System.err.println("Unable to send request.");
            }

            try {
                Thread.sleep(PERIOD);
            } catch (InterruptedException e) {
                System.err.println("Thread sleeping interrupted.");
            }
        }
    }
}
