package ijav.chat.client;

import ijav.chat.library.Message;
import javax.swing.*;
import java.io.*;
import java.util.Arrays;

public class ClientListener implements Runnable {
    private PrintStream pw;
    private ObjectInputStream inObj;
    private JList onlineList;

    public ClientListener(ObjectInputStream inObj, PrintStream pw, JList onlineList) {
        this.inObj = inObj;
        this.pw = pw;
        this.onlineList = onlineList;
    }

    @Override
    public void run() {
        Message message;
        try {
            while (true) {
                message = (Message) inObj.readObject();
                processMessage(message);
            }
        } catch (IOException | ClassNotFoundException e) { }
    }

    public void processMessage(Message message) {
        if (message.getNickname().equalsIgnoreCase("listOnline")) {
            updateOnlineList(message);
        } else {
            pw.println(message);
        }
    }

    private synchronized void updateOnlineList(Message message) {
        String[] users = message.getContent().split(";;");
        Arrays.sort(users);

        DefaultListModel model = new DefaultListModel();
        for (String user : users) {
            model.addElement(user);
        }

        onlineList.setModel(model);
    }
}
